---
title: آشنایی با فرایند ساخت فونت آزاد
event_date: 2024-04-08 20:00:00 +0330
presenter: عرفان خیراللهی
location: متعاقبا اعلام خواهد شد
geo:
featured:
key_people:
  - عرفان خیراللهی
  - مصطفی آهنگرها
---

در این جلسه ضمن آشنایی با فرایند ساخت فونت،
برای مشارکت در تکمیل قلم‌های ایغور برای زبان فارسی آماده خواهیم شد.

## منابع
- [نرم‌افزار فونت‌فورج](https://fontforge.org){:target="_blank"}
- [راهنمای انگلیسی فونت‌فورج](http://designwithfontforge.com){:target="_blank"}
- [راهنمای فارسی فونت‌فورج](https://github.com/ahangarha/designwithfontforge.com/tree/add-persian-translation/fa-IR){:target="_blank"} (در حال ترجمه)
- [قلم‌های آزاد اویغور](https://www.ukij.org/fonts/){:target="_blank"}
- [ویدئوی امین عابدی](https://www.youtube.com/watch?v=EcGGR0b4dMY){:target="_blank"}
- [راهنمای مختصر فونت‌فورج - مهدی صادقی](https://mehdix.ir/fontforge.html){:target="_blank"}

## رسانه

<iframe
  src="https://archive.org/embed/dona-02"
  class="w-full aspect-video bg-gray-200"
  frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen>
</iframe>
